/**
 * 新建邀请码
 * @param {Object} 数据库管理器
 * @param {String} username
 * @param {String} password
 */
$.newInviteCode = async function(db, username, password) {
	var db1 = db.new("user_account", "user_id");
	var date = new Date();
	var str = (date.toISOString() + username + password).md5();

	var n = 8;
	var len = n.rand(6);
	var code = "";
	for (var i = 0; i < 20; i++) {
		var invite_code = str.substring(0, len).toLocaleLowerCase();
		var count = await db1.count({
			invite_code
		}, false);
		if (count === 0) {
			code = invite_code;
			break;
		}
	}
	return code;
};

/**
 * @description 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	var by = ctx.request.body;
	var {
		mode,
		username,
		account,
		phone,
		email,
		password,
		nickname,
		code,
		invite_code,
		activate_code
	} = by;
	if (!phone && account) {
		phone = account;
	}
	if (!username && account) {
		username = account;
	}

	// 判断用户是否已存在
	var db1 = db.new("user_account", "user_id");
	var count = await db1.count({
		username
	}, "", "", false);
	if (count > 0) {
		return $.ret.error(10007, '注册失败，用户名已存在！');
	}
	if (phone) {
		count = await db1.count({
			phone
		});
		if (count > 0) {
			return $.ret.error(10007, '注册失败，该手机号已绑定其他账户！');
		}
	}
	if (email) {
		count = await db1.count({
			email
		});
		if (count > 0) {
			return $.ret.error(10007, '注册失败，该邮箱已绑定其他账户！');
		}
	}

	var key;
	if (mode == 1) {
		key = phone;
	} else if (mode == 2) {
		key = email;
	}
	if (key) {
		var code_check = await $.getCode("sign_up", key);
		if (!code_check) {
			return $.ret.error(30001, '请先获取验证码！');
		}
		if (code !== code_check) {
			return $.ret.error(30007, '验证码错误！');
		}
	}

	var conf = await $.config.conf();

	// 添加邀请码
	var referee_id = 0;
	if (invite_code) {
		var inviter = await db1.getObj({
			invite_code
		}, "", "", false);
		if (inviter) {
			referee_id = inviter.user_id;
		} else {
			return $.ret.error(70001, '邀请人不存在！');
		}
	} else if (conf.register_mode == 2) {
		return $.ret.error(30001, '邀请码(invite_code)是必须的！');
	}

	// 验证激活码
	var db2 = db.new("source_activate_code", "code_id");
	var activation;
	if (activate_code) {
		activation = await db2.getObj({
			state: 1,
			group: "注册码",
			activate_code
		}, "", "", false);
		if (!activation) {
			return $.ret.error(30002, '无效的注册码！');
		}
	} else if (conf.register_mode == 3) {
		return $.ret.error(30001, '注册码(activate_code)是必须的！');
	}

	var salt = password.substring(0, 6);
	var pass = (password + salt).md5();

	// 生成邀请码
	var icode = await $.newInviteCode(db, username, pass);

	var index = await db1.add({
		username,
		salt,
		phone,
		email,
		nickname,
		referee_id,
		password: pass,
		invite_code: icode
	});

	if (index > 0) {
		var user;
		if (mode == 1) {
			user = await db1.getObj({
				phone
			}, "", "", false);
		} else if (mode == 2) {
			user = await db1.getObj({
				email
			}, "", "", false);
		} else {
			user = await db1.getObj({
				username
			}, "", "", false);
		}

		if (user) {
			var db3 = db.new("user_count", "user_id");
			var user_id = user.user_id;
			await db3.add({
				user_id
			});
		}
		if (activation) {
			var now = new Date();
			var time_activate = now.toStr('yyyy-MM-dd hh:mm:ss');
			var time_validity = now.addDays(activation.period).toStr('yyyy-MM-dd hh:mm:ss');
			await db2.set({
				code_id: activation.code_id
			}, {
				state: 3,
				user_id: user.user_id,
				time_activate,
				time_validity
			});
			await $.activate_flow(db, user.user_id, user.user_id, "使用", activation.activate_code, "注册激活");
		}
		return $.ret.bl(true, '注册成功');
	} else {
		return $.ret.error(10000, '数据库业务逻辑错误。 ' + JSON.stringify(db1.error, true));
	}
};

exports.main = main;