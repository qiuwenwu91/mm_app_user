/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	await db.tpl.runFunc('seo', ctx.request);
	return db.tpl.view('./user/login.html');
};

exports.main = main;
