/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;
	var ret;
	var { address } = body;
	db.table = "user_account";
	db.key = "user_id";
	var obj = await db.getObj({
		wallet_address: address
	});
	if(!obj){
		await db.add({
			wallet_address: address,
			login_ip: ctx.ip
		});
	}
	else {
		obj.login_ip = ctx.ip;
		var time = new Date();
		obj.login_time = time.toStr('yyyy-MM-dd hh:mm:ss');
	}
	return $.ret.bl(true, "sign in success!");
};

exports.main = main;
