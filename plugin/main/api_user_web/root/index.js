var dir = "/static/template/default/".fullname();

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	var m = {};
	m.path = ctx.path.replace("/user", "");
	m.query = $.toUrl(ctx.request.query);
	var path = ctx.path;
	var user = await this.get_state(ctx, db) || {
		user_id: 0,
		gm: 0,
		vip: 0,
		avatar: "",
		nickname: "",
		username: "",
		phone: ""
	};
	if (ctx.path == "/user/") {
		path = ctx.path + "root";
		m.path = 'index';
		if (user.user_id) {
			var db1 = db.new('user_info', 'user_id');
			var info = await db1.getObj({
				user_id: user.user_id
			});
			if (info) {
				var avatar = user.avatar;
				user = Object.assign({}, user, info);
				user.avatar = info.avatar || avatar;
			}
		}
	}
	m.user = user;
	await db.tpl.runFunc('seo', ctx.request);
	var body = db.tpl.view(`.${path}.html`, m);
	if (!body) {
		var file = "./" + m.path + ".html";
		if (file.fullname(dir).hasFile()) {
			body = db.tpl.view('./user/root.html', m);
		} else {
			var arr = m.path.split('/');
			var app_name = arr[1];
			var apis = $.pool.api[app_name + "_web"];
			if (apis) {
				var lt = apis.list;
				var has = false;
				for (var i = 0, o; o = lt[i++];) {
					if (o.config.state === 1 && m.path.has(o.config.path)) {
						has = true;
						break;
					}
				}
				if (has) {
					body = db.tpl.view('./user/root.html', m);
				}
			}
		}
	}
	return body;
};

exports.main = main;