/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器, 如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	var pm = Object.assign({}, query, body);
	// 操作sql模板
	var {
		username,
		phone,
		code
	} = pm;
	var user = await this.get_state(ctx, db);
	if (user.username !== username) {
		return $.ret.error(10000, "注销失败！原因：申请注销的账户非本人所有");
	}

	var key = "code_unsubscribe_" + phone;
	var value = await $.cache.get(key);
	if (!value) {
		return $.ret.error(60001, "请先发送验证码！");
	}
	if (value !== code) {
		return $.ret.error(70001, "验证码错误！");
	}
	
	var user_id = user.user_id;
	db.table = "user_account";
	var bl = await db.del({
		user_id,
		username
	});
	if (bl > 0) {
		// 删除用户信息
		db.table = "user_info";
		await db.del({
			user_id
		});

		// 删除用户积分
		db.table = "user_count";
		await db.del({
			user_id
		});

		// 删除用户微信信息
		db.table = "wechat_info";
		await db.del({
			user_id
		});
		
		// // 删除录入的用户信息
		// db.table = "face_enter";
		// await db.del({
		// 	user_id
		// });

		// // 删除开门记录
		// db.table = "face_log";
		// await db.del({
		// 	user_id
		// });

		// // 删除考勤记录
		// db.table = "face_attendance";
		// await db.del({
		// 	user_id
		// });
		ctx.session.user = null;
		var token = ctx.headers[$.dict.token];
		if (token) {
			$.cache.del($.dict.session_id + '_' + token);
			var nickname = await $.cache.get("ofToken_" + token);
			$.cache.del("ofName_" + nickname);
			$.cache.del("ofToken_" + token);
		}
		return $.ret.bl("注销成功！");
	}
	return $.ret.error(10000, "注销失败！原因：数据库连接失败");
};

exports.main = main;