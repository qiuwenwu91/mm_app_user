/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;
	var date = new Date();
	var user_date = [];
	var len = 12;

	var db2 = db.new("user_account", "user_id");
	db2.size = 0;
	for (var i = 0; i < len; i++) {
		var time = date.addDays(i - len + 1);

		var lt = await db2.groupCount({
			time_create_min: time.toStr('yyyy-MM-dd 00:00:00'),
			time_create_max: time.toStr('yyyy-MM-dd 23:59:59'),
		}, 'user_id', 'user_id');

		// 实到人数
		var value = 0;
		if (lt.length) {
			value = lt.length;
		}

		user_date.push({
			time: time.toStr('yyyy-MM-dd'),
			value
		});
	}

	sql_str = "SELECT group_id, count(user_id) as value FROM user_account group by group_id order by group_id asc;";
	var user_group = await db.run(sql_str);

	var db1 = db.new("user_group", "group_id");
	db1.size = 0;
	var list = await db1.get();
	for (var i = 0; i < user_group.length; i++) {
		var o = user_group[i];
		var obj = list.getObj({
			group_id: o.group_id
		});
		if (obj) {
			o.name = obj.name;
		}
	}
	return $.ret.body({
		user_date,
		user_group
	});
};

exports.main = main;