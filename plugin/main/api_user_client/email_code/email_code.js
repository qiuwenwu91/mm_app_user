class Email_code {
	/**
	 * 构造函数
	 * @param {Object} config 配置参数
	 */
	constructor(config) {
		this.config = {
			// 验证码有效时长
			expires: 15,
			// 短信发送地址
			url: "http://api.smsbao.com/sms?u=${username}&p=${password}&m=${email}&c=${content}",
			// 用户名
			username: "31772309",
			// 密码
			password: "asd159357"
		}
	}
}

/**
 * 新建验证码
 * @param {String} method 请求方法
 * @param {String} email 邮箱
 * @return {String} 返回验证码
 */
Email_code.prototype.new_code = async function(key) {
	var code;
	var num = Math.ceil(Math.random() * 100000);
	if (num < 10) {
		code = "0" + num + "0" + num;
	} else if (num < 100) {
		code = "0000" + num;
	} else if (num < 1000) {
		code = "000" + num;
	} else if (num < 10000) {
		code = "00" + num;
	} else if (num < 100000) {
		code = "0" + num;
	}
	await $.cache.set(key + "_click", code, 60);
	var bl = await $.cache.set(key, code, this.config.expires * 60);
	if (bl) {
		return code;
	} else {
		return null;
	}
};

/**
 * 发送验证码
 * @param {String} method 请求方法
 * @param {String} email 邮箱
 * @return {Object} 返回发送结果
 */
Email_code.prototype.send = async function(method, email) {
	var {
		username,
		password,
		expires,
		url
	} = this.config;
	var key = "code_" + method + "_" + email;
	var value = await $.cache.get(key + "_click");
	var code;
	if (value) {
		return $.ret.bl(true, "已发送验证码，请注意查收！");
	} else {
		code = await this.new_code(key);
	}
	if (code) {
		password = password.md5();
		var web_name = await $.config.get('web_name');
		// 发送验证码
		var content = encodeURIComponent(`【${web_name}】您的验证码为${code}，在${expires}分钟内有效。`);
		var u = eval("`" + url + "`");
		var http = new $.Http();
		var web = await http.get(u);
		var errno = web.body;
		if (errno) {
			errno = Number(errno);
			if (errno == 0) {
				return $.ret.bl(true, "发送成功！");
			} else {
				var meaage = "";
				switch (errno) {
					case -1:
						meaage = "参数不全"
						break
					case -2:
						meaage = "服务器空间不支持,请确认支持curl或者fsocket，联系您的空间商解决或者更换空间！"
						break
					case 30:
						meaage = "密码错误";
						break;
					case 40:
						meaage = "账号不存在";
						break;
					case 41:
						meaage = "余额不足";
						break;
					case 43:
						meaage = "IP地址受限";
						break;
					case 50:
						meaage = "内容含有敏感词";
						break;
					case 51:
						meaage = "邮箱不正确";
						break;
				}
				return $.ret.error(11000 + errno, meaage);
			}
		} else {
			return $.ret.error(10000, "请求短信服务端失败！");
		}
	} else {
		return $.ret.error(10000, "缓存验证码失败！");
	}
};

/**
 * 发送验证码
 * @param {String} query 查询参
 * @param {Object} db 数据库管理器
 * @return {Object} 执行结果
 */
Email_code.prototype.set_delivery_address = async function(query, db, ctx) {
	var {
		username,
		email
	} = query;
	var user = await db.getObj({
		username
	});
	if (user) {
		return $.ret.bl(false, "用户已存在！");
	}
	return await this.send("set_delivery_address", email);
};

/**
 * 注册、登录
 * @param {String} query 查询参
 * @param {Object} db 数据库管理器
 * @return {Object} 执行结果
 */
Email_code.prototype.sign_up = async function(query, db, ctx) {
	var {
		email
	} = query;
	var user = await db.getObj({
		email
	});
	if (user) {
		return $.ret.error(10001, '该邮箱已被使用，请换一个！');
	}
	return await this.send("sign_up", email);
};

/**
 * 登录
 * @param {String} query 查询参
 * @param {Object} db 数据库管理器
 * @return {Object} 执行结果
 */
Email_code.prototype.sign_in = async function(query, db, ctx) {
	var {
		email
	} = query;
	var user = await db.getObj({
		email
	});
	if (!user) {
		return $.ret.error(10000, '用户不存在！');
	}
	return await this.send("sign_in", email);
};

/**
 * 忘记密码
 * @param {String} query 查询参
 * @param {Object} db 数据库管理器
 * @return {Object} 执行结果
 */
Email_code.prototype.forgot = async function(query, db, ctx) {
	var {
		username,
		email
	} = query;
	var user = await db.getObj({
		email
	});
	if (!user) {
		return $.ret.error(10000, "用户不存在！");
	}
	return await this.send("forgot", email);
};

/**
 * 修改密码
 * @param {String} query 查询参
 * @param {Object} db 数据库管理器
 * @return {Object} 执行结果
 */
Email_code.prototype.password = async function(query, db, ctx) {
	var {
		username,
		email
	} = query;
	var user = ctx.session.user
	if (!user) {
		return $.ret.error(10000, "账号未登录！");
	}
	var email = user.email
	return await this.send("password", email);
};


/**
 * 发送验证码 - 设置支付密码
 * @param {String} query 查询参
 * @param {Object} db 数据库管理器
 * @return {Object} 执行结果
 */
Email_code.prototype.pay_password = async function(query, db, ctx) {
	var {
		email
	} = query;
	var user = ctx.session.user;
	if (!user) {
		return $.ret.error(6000, "账户未登录！");
	}

	if (user.email !== email) {
		return $.ret.error(10000, "输入的号码与绑定的邮箱不同！");
	}
	return await this.send("pay_password", email);
};

/**
 * 绑定手机
 * @param {String} query 查询参
 * @param {Object} db 数据库管理器
 * @return {Object} 执行结果
 */
Email_code.prototype.bind = async function(query, db, ctx) {
	var {
		username,
		email
	} = query;
	var user = await db.getObj({
		username
	});
	if (!user) {
		return $.ret.error(10000, "用户不存在！");
	} else {
		email = user.email;
	}
	return await this.send("bind", email);
};

/**
 * 注销账户
 * @param {String} query 查询参
 * @param {Object} db 数据库管理器
 * @return {Object} 执行结果
 */
Email_code.prototype.unsubscribe = async function(query, db, ctx) {
	var {
		email
	} = query;
	var user = ctx.session.user;
	if (!user) {
		return $.ret.error(6000, "账户未登录！");
	}
	if (!email) {
		return $.ret.error(10000, "邮箱（email）不能为空！");
	}
	if (user.email !== email) {
		return $.ret.error(10000, "输入的号码与绑定的邮箱不同！");
	}
	return await this.send("unsubscribe", email);
};

/**
 * 执行
 * @param {Object} query
 * @param {Object} db
 */
Email_code.prototype.run = async function(query, db, ctx) {
	var method = query.method;
	var param = Object.assign({}, query, ctx.request.body);
	if (this[method]) {
		return await this[method](param, db, ctx);
	} else {
		return $.ret.error(10000, "错误的请求！");
	}
};

module.exports = Email_code;