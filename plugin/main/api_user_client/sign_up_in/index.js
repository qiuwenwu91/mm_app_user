/**
 * @description 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	db.table = "user_account";

	var by = ctx.request.body;
	var {
		username,
		phone,
		code,
		openid,
		info
	} = by;

	// 短信验证码判断
	if (code) {
		var key = "code_sign_up_in_" + phone;
		var value = await $.cache.get(key);
		if (!value) {
			return $.ret.error(10000, '请先发送验证码！');
		}
		var json = value.toJson();
		if (code !== json.code) {
			return $.ret.bl(false, '验证码不正确！');
		}
	}

	// 判断用户是否已存在
	var user = await db.getObj({
		username
	});
	if (!user) {
		//没有账号进行注册
		var nickname = info.nickName
		var avatar = info.avatarUrl
		var index = await db.add({
			username,
			phone,
			nickname,
			avatar
		});
		if (index > 0) {
			user = await db.getObj({
				username
			});
			if (user) {
				var user_id = user.user_id;
				db.table = "user_count";
				await db.add({
					user_id,
					coin: 0
				});
				db.table = "user_info";
				await db.add({
					user_id
				});
				db.table = "user_sns";
				await db.add({
					user_id
				});
			} else {
				return $.ret.error(10000, '数据库业务逻辑错误。 ' + JSON.stringify(db.error, true));
			}
		} else {
			return $.ret.error(10000, '数据库业务逻辑错误。 ' + JSON.stringify(db.error, true));
		}
	}
	// 进行登录
	ctx.session.user = user;
	var token = ctx.session.uuid;
	var login_period = await $.config.get('login_period') || 7200;
	// 7200为2小时
	$.cache.set("ofName_" + user.nickname, token, login_period);
	$.cache.set("ofToken_" + token, user.nickname, login_period);
	$.cache.set($.dict.session_id + "_" + token, user, login_period);

	db.table = "user_state";
	var state = await db.get({
		user_id
	});
	if (state) {
		await db.set({
			user_id
		}, {
			token,
			times: state.times + 1
		});
	} else {
		await db.add({
			user_id
		}, {
			token,
			times: 1
		});
	}

	var body = $.ret.body({
		token,
		user
	});

	//绑定微信信息
	var user_id = user.user_id;
	if (!openid) {
		return $.ret.error(10000, '数据库业务逻辑错误。 ' + JSON.stringify(db.error, true));
	}
	var way = by.way || "wechat";
	db.table = way + "_info";
	var wx_info = await db.getObj({
		openid: openid
	});
	info = JSON.stringify(by.info)
	if (wx_info) {
		var obj = {
			info,
			user_id,
			openid: openid
		}
		db.set({
			openid: openid
		}, obj);
	} else {
		db.add({
			info,
			user_id,
			openid: openid
		});
	}

	return body;
};

exports.main = main;