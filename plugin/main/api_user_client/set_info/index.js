
/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;
	
	// console.log("运行修改性别" ,query ,body);
	var user = ctx.session.user;
	if(!user){
		return $.ret.error(10000 ,"用户未登录！");
	}
	var user_id = user.user_id;
	
	var sex = query.sex || body.sex;
	if(!sex){
		return $.ret.error(10000 ,"性别是必填字段！");
	}
	
	var db1 = Object.assign({} ,db);
	db1.table = "user_info";
	
	// console.log("操作", user_id ,sex ,db1.sql ,db1.error);
	var bl = await db1.set({user_id} ,{sex});
	
	bl = bl > 0 ? true : false;
	var message = "性别修改失败！";
	
	if(bl){
		message = "性别修改成功！";
	}
	
	return $.ret.bl(bl ,message);
};

exports.main = main;
