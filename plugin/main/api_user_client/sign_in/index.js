/**
 * 账户缓存
 */
$.state_cache = async function(ctx, db, user) {
	if (!user) {
		return $.ret.error(10000, '账号不存在');
	}
	var login_ip = ctx.ip.replace('::ffff:', '');
	var user_id = user.user_id;
	var time = (new Date()).toStr('yyyy-MM-dd hh:mm:ss');
	// 记录当前登录所用的IP地址
	await db.set({
		user_id
	}, {
		login_ip,
		login_time: time
	});

	ctx.session.user = user;
	var token = ctx.session.uuid;
	var login_period = await $.config.get('login_period') || 7200;
	// 7200为2小时
	$.cache.set("ofName_" + user.nickname, token, login_period);
	$.cache.set("ofToken_" + token, user.nickname, login_period);
	$.cache.set($.dict.session_id + "_" + token, user, login_period);

	db.table = "user_state";
	db.key = "user_id";
	var state = await db.getObj({
		user_id
	});

	if (state) {
		await db.set({
			user_id
		}, {
			token,
			times: state.times + 1
		});
	} else {
		await db.add({
			user_id,
			token,
			times: 1
		});
	}
	user.token = token;
	var u = Object.assign({}, user);
	u.password_pay = u.password_pay ? "******" : "";
	u.password = u.password ? "******" : "";
	delete u.salt;
	delete u.time_create;

	// 自动生成的uuid是通过IP和浏览器信息加密而成，如果需要解密确认其身份，可再加上user_id加密，自行生成uuid
	var body = $.ret.body(user);
	return body
}

/**
 * 账户登录
 */
async function account_sign_in(ctx, db, account, password) {
	// 如果登陆方式为1，则使用第三方社交账号和本站密码进行登录
	if (!password) {
		return $.ret.error(30002, "密码(password)不能为空");
	}
	db.table = "user_account";
	db.key = "user_id";
	// 使用用户名登录
	var users = await db.getSql(`phone='${account}' || username='${account}' || email='${account}'`);
	if (users.length <= 0) {
		return $.ret.error(60001, "用户不存在");
	}
	var user = users[0];
	var pass = (password + user.salt).md5();
	if (pass !== user.password) {
		return $.ret.error(30003, "密码(password)错误");
	}
	return await $.state_cache(ctx, db, user);
}

/**
 * 快捷注册并登录
 */
async function fast_sign_in(db, params, user) {
	var info = params.info;
	if (info) {
		var json_info = JSON.parse(info);
		user.nickname = json_info.nickName;
		user.avatar = json_info.avatarUrl;
	}
	await $.bind_account(db, params, user);
}

/**
 * 手机号码登录
 */
async function phone_sign_in() {
	// 使用手机号+验证码登录
	db.table = "user_account";
	db.key = "user_id";
	var code = params['code']
	var phone = account;
	if (!code) {
		return $.ret.error(10000, "验证码(code)不能为空！");
	}
	// 短信验证码判断
	var key = "code_sign_" + phone;
	var value = await $.cache.get(key);
	if (!value) {
		return $.ret.error(10000, '请先发送验证码！');
	}
	var json = value.toJson();
	if (code !== json.code) {
		return $.ret.error(30000, '验证码不正确！');
	}
	var user = await db.getObj({
		phone
	});
	if (!user) {
		var username = phone;
		//没有账号，去注册
		var invite_code = await $.getCode(db, username, password);
		var index = await db.add({
			username,
			phone,
			invite_code
		});
		if (index > 0) {
			var user = await db.getObj({
				username
			});
			if (user) {
				var user_id = user.user_id;
				db.table = "user_count";
				await db.add({
					user_id
				});
			}
		}
	}
	return await $.state_cache(ctx, db, user);
}

/**
 * 社交账号登录
 */
async function sns_sign_in() {
	// 如果登陆方式为1，则使用第三方社交账号和本站密码进行登录
	if (!password) {
		return $.ret.error(30002, "密码(password)不能为空");
	}
	db.table = "user_sns";
	var mm = params["mm"];
	var arr = [];
	if (mm) {
		// 使用超级美眉账号登录
		arr = await db.get({
			mm
		});
	} else {
		var qq = params["qq"];
		if (qq) {
			// 使用QQ账号登录
			arr = await db.get({
				qq
			});
		} else {
			var baidu = params["baidu"];
			if (baidu) {
				// 使用百度账号登录
				arr = await db.get({
					baidu
				});
			} else {
				var taotao = params["taotao"];
				if (taotao) {
					// 使用淘宝账号登录
					arr = await db.get({
						taotao
					});
				} else {
					var wechat = params["wechat"];
					if (wechat) {
						// 使用微信账号登录
						arr = await db.get({
							wechat
						});
					} else {
						return $.ret.error(30000, '不支持的SNS账号');
					}
				}
			}
		}
	}
	if (arr.length > 0) {
		var o = arr[0];
		db.table = "user_sns";
		user = await db.getObj({
			user_id: o.user_id
		});
	}
	return await $.state_cache(ctx, db, user);
}

/**
 * 关联创建
 */
async function sns_sign_in() {

}

$.bind_account = async function(db, body, user) {
	var openid = body.openid;
	var user_id = user.user_id;
	if (openid) {
		var way = body.way || "wechat";
		db.table = way + "_info";
		var info = await db.getObj({
			openid
		});
		if (info) {
			var obj = {
				user_id,
				openid,
				info: body.info
			}
			await db.set({
				openid
			}, obj);
		} else {
			await db.add({
				user_id,
				openid,
				info: body.info
			});
		}
		return user;
	}
}

/**
 * @description 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	var params = {};
	if (ctx.method === "POST") {
		params = ctx.request.body;
	} else {
		params = ctx.request.query;
	}
	if (ctx.session.user) {
		ctx.session.user = null;
	}
	var password = params['password'];
	var account = params["account"];


	// 获取登录方式
	var method = params["method"];
	// $.log.debug('登录方式', method);
	if (!method) {
		return await account_sign_in(ctx, db, account, password);
	} else if (method === 'phone') {
		return await phone_sign_in(ctx, db, account, password);
	} else if (method === "sns") {
		return await sns_sign_in(ctx, db, account, password);
	}
};

exports.main = main;