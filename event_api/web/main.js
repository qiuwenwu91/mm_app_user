// 使用api管理器
var api = $.api_admin('user_web', '网站-用户管理');
// 首次启动更新api接口;
api.update();

/**
 * @description 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	if (ctx.path === '/user') {
		ctx.response.redirect('/user/');
		return
	}
	if (ctx.path.indexOf("/user/img") === 0) {
		return null;
	}
	// 使用模板引擎
	db.tpl = new $.Tpl();
	var bag = db.tpl.viewBag;
	bag.path = ctx.path;
	bag.query = ctx.query;
	bag.app = "user";
	await db.tpl.runFunc('common', ctx.request);
	await db.tpl.runFunc('user', ctx.request);

	// 在这定义要访问的数据库 (分布式开发时设置不同的数据库名)
	$.push(db, $.sql.db(), true);
	db.tpl.current_theme = bag.current_theme;
	var html = await api.run(ctx, db);
	if (html) {
		html = await $.func.html_filter(ctx, db, html);
		return await $.func.vue_filter(html);
	}
};

exports.main = main;