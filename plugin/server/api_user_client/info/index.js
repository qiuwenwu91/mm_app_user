/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;
	var user = await this.get_state(ctx, db) || {
		user_id: 0
	};
	// 操作sql模板
	var m = query.method;
	switch (m) {
		case "set":
			query.user_id = user.user_id;
			var by = {};
			if (body.phone) {
				by.phone = body.phone;
				delete body.email;
			}
			if (body.email) {
				by.email = body.email;
				delete body.email;
			}
			if (by) {
				
			}
			break;
		default:
			break;
	}
	var ret = await this.sql.run(query, body, db);
	return ret;
};

exports.main = main;