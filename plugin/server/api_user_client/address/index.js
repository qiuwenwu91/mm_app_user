/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	// 用户端接口只允许用户操作自己的地址，所以先将查询条件中加入用户ID
	var user = await this.get_state(ctx, db);
	query.user_id = user.user_id;

	// 操作sql模板
	var m = query.method;
	var is_default;
	var address_id = 0;
	if (m === "add") {
		body.user_id = user.user_id;
		is_default = body.is_default;
	} else if (m === "del") {

	} else if (m === "set_default") {
		var db1 = db.new("user_address", "address_id");
		delete query.method;
		var params = Object.assign(query, body);
		// 先查询地址是否存在
		var obj = await db1.getObj(params);
		if (obj) {
			// 存在则进行修改
			// 先将所有地址的is_default设置为0
			var bl = await db1.set({
				user_id: user.user_id
			}, {
				is_default: 0
			});
			if (bl > 0) {
				bl = await db1.set(params, {
					is_default: 1
				});
				if (bl > 0) {
					return $.ret.bl(true, "设置成功！");
				}
			}
			return $.ret.error(10000, "设置失败！原因：无法操作数据库");
		} else {
			// 不存在则不进行修改
			return $.ret.error(30001, "错误的地址ID(address_id)");
		}
	} else if (m === "set") {
		address_id = query.address_id;
		is_default = body.is_default;
	} else {

	}
	var ret = await this.sql.run(query, body, db);
	if (is_default) {
		var db2 = db.new("user_address", "address_id");
		if (!address_id) {
			var addr = await db2.getObj({}, 'address_id desc');
			if (addr) {
				address_id = addr.address_id;
			}
		}
		bl = await db2.set({
			address_id_not: address_id
		}, {
			is_default: 0
		});
	}
	return ret;
};

exports.main = main;