/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;
	var {
		avatar = "",
			nickname = "",
			name = "",
			phone = "",
			username = "",
			address = "",
			pos_x = 0,
			pos_y = 0
	} = body;

	var user = await this.get_state(ctx, db);

	if (!user) {
		return $.ret.error(10000, "账户未登录！");
	}
	var user_id = user.user_id;

	var obj = {};
	// 操作sql模板
	var m = req.method;
	if (m === "POST") {
		db.table = "user_account";
		if (username) {
			var u = await db.getObj({
				username
			});
			if (u.user_id !== user_id) {
				return $.ret.error(10000, "该账号已被他人使用");
			}
			await db.set({
				user_id
			}, {
				username
			});
			user.username = username;
		}
		if (phone) {
			var u = await db.getObj({
				phone
			});
			if (u.user_id !== user_id) {
				return $.ret.error(10000, "该手机号码已被他人使用");
			}
			await db.set({
				user_id
			}, {
				phone
			});
			user.phone = phone;
		}
		await db.set({
			user_id
		}, {
			avatar,
			nickname
		});
		ctx.session.user = user;
		return $.ret.bl(true, "修改成功！");
	} else {
		obj = {
			avatar,
			nickname,
			name,
			phone,
			address,
			pos_x,
			pos_y
		};
		db.table = "user_account";
		var obj_1 = await db.getObj({
			user_id
		});
		$.push(obj, obj_1);
		return $.ret.obj(obj);
	}
};

exports.main = main;