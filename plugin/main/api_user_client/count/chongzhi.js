
/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;
	
	db.table = "user_count";
	db.key = "user_id";
	
	var token = ctx.headers[$.dict.token];
	if (!token) {
		return $.ret.error(70000, '没有访问权限');
	}
	var user = await $.cache.get($.dict.session_id + '_' + token);
	if (!user) {
		return $.ret.error(50000, '账户未登录');
	}
	var { user_id } = user;
	
	// 操作sql模板
	var m = query.method;
	var count = await db.getObj({ user_id });
	if (m === "transfer") {
		var { to_address, amount } = body;
		if(count.coin <= amount) {
			return $.ret.error(70001, '转账失败！原因：账户余额不足');
		}
		var db2 = db.new();
		db2.table = "user_account";
		var to_user = await db2.getOBj({
			wallet_address: to_address
		});
		if(!to_user) {
			return $.ret.error(70002, '转账失败！原因：收款账户不存在！');
		}
		// 扣掉用户的积分
		count.coin -= amount;
		
		// 给收款用户增加积分
		var to_count = await db.getObj({ user_id: to_user.user_id });
		to_count.coin += amount;
		return $.ret.bl(true, '转账成功！账户余额：' + count.coin);
	} else if (m === "deposit") {
		var { log_id } = body;
		var obj = await $.ct.send('bank', 'USDT', 'getLog', 0, log_id);
		if(!obj || !obj.log_id){
			return $.ret.error(10001, '充值失败！原因：智能合约没有找到充值记录');
		}
		if(obj.user_id !== user_id){
			return $.ret.error(10001, '系统错误！原因：实际充值账户与当前账户不符');
		}
		var db2 = db.new();
		db2.table = "user_deposit_log";
		var obj = await db2.getOBj({
			log_id
		});
		if(!obj) {
			return $.ret.error(10001, '已充值到账，无需再刷新！');
		}
		var amount = $.ct.toNum(obj.amount, $.ct.token.USDT.decimals);
		await db2.add({
			log_id,
			user_id,
			amount
		});
		count.coin += amount;
		return $.ret.bl(true, '充值成功！目前余额：' + count.coin);
	} else if (m === "withdraw") {
		var { amount } = body;
		if(count.coin < amount) {
			return $.ret.error(70001, '提现失败！原因：账户余额不足');
		}
		var balance = await $.ct.call('token', 'USDT', 'balanceOf', 0, $.ct.address);
		sys_balance = $.ct.toNum(balance, $.ct.token.USDT.decimals);
		if(sys_balance < amount) {
			return $.ret.error(70001, '提现失败！原因：系统错误，请联系管理员');
		}
		// 扣掉用户的积分
		count.coin -= amount;
		
		// 调用合约给用户转账
		var amountStr = $.ct.toBN(amount, $.ct.token.USDT.decimals);
		var tx = await $.ct.send('bank', 'USDT', 'transfer', 0, user.wallet_address, amountStr);
		if(!tx){
			return $.ret.error(70001, '提现失败！原因：系统错误，请联系管理员');
		}
		return $.ret.bl(true, '提现成功！账户余额：' + count.coin);
	} else {
		return $.ret.obj(count);
	}
	return ret;
};

exports.main = main;
