/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	// 操作sql模板
	var m = query.method;
	if (m === "add") {

	} else if (m === "del") {

	} else if (m === "set_defalut") {
		if (query.address_id) {
			var db1 = db1.new("user_address", "address_id");
			// 先查询地址是否存在
			var obj = await db1.getObj(query);
			if (obj) {
				// 存在则进行修改
				// 先将所有地址的is_default设置为0
				var bl = await db1.set({
					user_id: obj.user_id
				}, {
					is_default: 0
				});
				if (bl > 0) {
					// 然后将要设置为默认的ID改为1
					obj.is_default = 1;
					return $.ret.bl(true, "设置成功！");
				} else {
					return $.ret.error(10000, "设置失败！原因：无法操作数据库");
				}
			} else {
				// 不存在则不进行修改
				return $.ret.error(30001, "错误的地址ID（address_id）！");
			}
		} else {
			return $.ret.error(30001, "地址ID（address_id）参数是必须的！");
		}
	} else if (m === "set") {

	} else {

	}
	var ret = await this.sql.run(query, body, db);
	return ret;
};

exports.main = main;