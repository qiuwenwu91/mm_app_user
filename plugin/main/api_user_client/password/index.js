/**
 * @description 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;
	var user = ctx.session.user;
	if (!user) {
		var token = ctx.headers[$.dict.token];
		if (token) {
			user = await $.cache.get($.dict.session_id + '_' + token);
		}
		if (!user) {
			return $.ret.error(10001, '账户未登录！');
		}
	}
	var {
		password_old,
		password,
		confirm_password,
		ticket
	} = body;

	var db1 = db.new("user_account", "user_id");
	var obj = await db1.getObj({
		username: user.username
	});
	if (!obj) {
		return $.ret.error(10001, '账户不存在！');
	}
	if (ticket) {
		if (ticket !== user.token.md5()) {
			return $.ret.error(10002, '身份验证失败，无法修改密码！');
		}
	} else {
		var pass = (password_old + user.salt).md5();
		if (pass !== obj.password) {
			return $.ret.error(10002, '原密码输入错误！');
		}
	}
	var password = (password + user.salt).md5();
	var user_id = user.user_id;
	var bl = await db1.set({
		user_id
	}, {
		password
	});

	if (bl > 0) {
		user.password = password;
		ctx.session.user = user;
		return $.ret.bl(true, "密码修改成功！");
	}
	return $.ret.bl(false, "密码修改失败！");
};

exports.main = main;