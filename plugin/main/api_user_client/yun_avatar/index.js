var fs = require('fs');

async function update_avatar(db, page, field) {
	db.page = page;
	var list = await db.get({}, '', field);
	for (var i = 0; i < list.length; i++) {
		var o = list[i];
		var av = o.avatar;
		if (av && av.indexOf("http") !== 0) {
			try {
				var file = ("/static" + av).fullname();
				if (file.hasFile()) {
					var buff = fs.readFileSync(file);
					var res = await $.cloud.upload(av, buff);
					if (res && res.Location) {
						db.set({
							user_id: o.user_id
						}, {
							avatar: "https://" + res.Location
						});
					}
				}
			} catch (e) {
				console.error(e);
			}
		}
	}
}

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器, 如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;
	var param = Object.assign({}, query, body);

	db.table = param.table || "user_account";
	var field = param.field || "avatar";

	var count = await db.count();
	var len = Math.ceil(count / db.size);
	for (var i = 0; i < len; i++) {
		await update_avatar(db, i + 1, "`user_id`,`" + field + "`");
	}
	return $.ret.bl(true, '已更新！');
}

exports.main = main;