/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		user_id
	} = req.query;

	var db1 = db.new("user_count", "user_id");
	var user = await db.getObj({
		user_id
	});
	if (!obj) {
		return $.ret.error(10000, "用户不存在！");
	}
	return $.ret.obj(obj);
};

exports.main = main;
