/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 初始化返回值
	var ret = $.ret.error(10000, "非法操作");
	// 获取请求参数
	var req = ctx.request;
	// 请求参数
	var query = req.query;
	// 执行方式
	var method = query.method;

	// 用户表操作
	db.table = "user_account";
	// 补全所有邀请码
	if (method == "update") {
		var users = await db.get();
		for (var i = 0; i < users.length; i++) {
			var o = users[i];
			if (!o.invite_code) {
				var user_id = o.user_id;
				// 生成邀请码
				var invite_code = createInvitationCode(o.username);
				await db.set({
					user_id
				}, {
					invite_code
				});
			}
		}
		ret = $.ret.bl(true, "补全邀请码成功!");
	}
	// 添加邀请码
	else if (method == "invite_code") {
		var user_id = query.user_id;
		var user = await db.getObj({
			user_id
		});
		// 生成邀请码
		var invite_code = createInvitationCode(user.username);
		// 修改验证码
		await db.set({
			user_id
		}, {
			invite_code
		});
		ret = $.ret.bl(true, "添加邀请码成功!");
	}
	// 绑定邀请对象
	else if (method == "binding") {
		// 创建邀请记录操作
		var db2 = new db();
		db.table = "user_invite";
		// 查询邀请记录
		var invite_code = query.invite_code;
		var user = await db.getObj({
			invite_code
		});
		// 邀请对象不存在
		if (!user) {
			return $.ret.error(10000, "邀请对象不存在!");
		}
		// 被邀请ID和邀请ID
		var invitee_id = ctx.session.user.user_id;
		var inviter_id = user.user_id;
		// 查询邀请记录
		var log = await db2.get({
			invitee_id,
			inviter_id
		});
		// 邀请已存在返回
		if (log) {
			return $.ret.error(10000, "已经进行过邀请了!");
		}
		// 添加邀请记录
		await db2.add({
			invitee_id,
			inviter_id
		});
		ret = $.ret.bl(true, "邀请成功!");
	}

	return ret;
};

/**
 *  生成邀请码
 *  @param {String} str
 */
function createInvitationCode(str) {
	var date = new Date();
	var timestamp = date.getTime();
	return (str + timestamp).md5().substr(-8);
}

exports.main = main;
