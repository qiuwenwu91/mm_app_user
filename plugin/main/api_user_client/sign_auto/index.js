/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;
	var ret;
	var pm = Object.assign({}, query, body);
	var {
		openid,
		register
	} = pm;
	if (!openid) {
		return $.ret.error(10000, "用户开放ID(oepn_id)不能为空！");
	}
	var db1 = db.new("wechat_info", "info_id");
	var db2 = db.new("user_account", "user_id");
	var info = await db1.getObj({
		openid
	});
	if (!info) {
		return $.ret.error(70001, "错误的openid");
	}
	var user;
	var user_id = 0;
	if (info.user_id) {
		user_id = info.user_id;
		user = await db2.getObj({
			user_id
		});
	}
	if (!user) {
		if (!register) {
			return $.ret.error(70002, "用户未注册！");
		} else {
			// 没有账号进行注册
			var username = openid.md5().substring(0, 10);
			var nickname = '微信用户';
			var index = await db2.add({
				username,
				nickname
			});
			if (index > 0) {
				user = await db2.getObj({
					username
				});
				if (user) {
					var user_id = user.user_id;
					db.table = "user_count";
					await db.add({
						user_id,
						coin: 0
					});
					db.table = "user_info";
					await db.add({
						user_id
					});
					db.table = "user_sns";
					await db.add({
						user_id
					});
				} else {
					return $.ret.error(10000, '数据库业务逻辑错误。 ' + JSON.stringify(db.error, true));
				}
			} else {
				return $.ret.error(10000, '数据库业务逻辑错误。 ' + JSON.stringify(db.error, true));
			}
		}
	}
	//进行登录
	ctx.session.user = user;
	var token = ctx.session.uuid;
	var login_period = await $.config.get('login_period') || 7200;
	// 7200为2小时
	$.cache.set("ofName_" + user.nickname, token, login_period);
	$.cache.set("ofToken_" + token, user.nickname, login_period);
	$.cache.set($.dict.session_id + "_" + token, user, login_period);

	var obj = await db1.getObj({
		user_id: user.user_id
	});
	if (obj) {
		await db1.set({
			user_id: user.user_id
		}, {
			openid
		});
	} else {
		var obj = await db1.getObj({
			openid
		});
		if (obj) {
			await db1.set({
				openid
			}, {
				user_id: user.user_id
			});
		} else {
			await db1.add({
				user_id: user.user_id,
				openid
			});
		}
	}

	db.table = "user_state";
	db.key = "user_id";
	var state = await db.getObj({
		user_id
	});

	if (state) {
		await db.set({
			user_id
		}, {
			token,
			times: state.times + 1
		});
	} else {
		await db.add({
			user_id,
			token,
			times: 1
		});
	}

	var ret = $.ret.body({
		token,
		user
	});
	return ret;
};

exports.main = main;