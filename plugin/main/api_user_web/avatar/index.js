const send = require('koa-send');

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	var path = ctx.path;
	var user_id = path.right('/user/avatar/');
	user_id = user_id.left('.', true);
	var file = "";
	if (user_id) {
		var db0 = db.new('user_account', 'user_id');
		var u = await db0.getObj({
			user_id
		});
		if (u) {
			file = u.avatar;
		}
	}
	if (file) {
		var url;
		if (file.indexOf("http") === -1) {
			var conf = await $.config.conf();
			var web_url = conf.web_url;
			if (web_url.endsWith('/') && file.indexOf("/") === 0) {
				file = file.replace('/', '')
			}
			url = web_url + file;
		} else {
			url = file;
		}
		if (url) {
			var http = new $.Http();
			var res = await http.get(url);
			var status = res.status;
			if (status === 200) {
				if (res.buffer.length) {
					ctx.set('Content-Type', res.headers['content-type']);
					ctx.set('Cache-Control', 'max-age=7200');
					ctx.body = res.buffer;
					return null;
				}
			}
		}
	}
};

exports.main = main;