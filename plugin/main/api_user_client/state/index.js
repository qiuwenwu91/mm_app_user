/**
 * @description 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	var o = await this.get_state(ctx, db);
	if (o) {
		return $.ret.body(o);
	}
	var token = ctx.headers[$.dict.token];
	if (token) {
		return $.ret.error(50000, '非法访问');
	}
	return $.ret.error(40000, '用户未登录');
}

exports.main = main;