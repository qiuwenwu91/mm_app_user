/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	var params = Object.assign({}, query, body);
	var {
		account,
		password,
		code
	} = params;
	var phone = params.phone || account;
	
	db.table = "user_account";
	db.key = "user_id";
	var user = await db.getObj({
		phone
	});
	
	if (!user) {
		return $.ret.error(10000, "用户不存在");
	} 
	user.password = (password + user.salt).md5();
	return $.ret.bl(true, '密码重置成功！');
};

exports.main = main;
