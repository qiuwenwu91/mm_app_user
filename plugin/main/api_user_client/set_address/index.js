
/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	var user = ctx.session.user;
	if (!user) {
		return $.ret.error(10000, "用户未登录!");
	}

	var user_id = user.user_id;
	body.user_id = user_id;

	db.table = 'user_address';
	var db1 = db.new('user_address', 'address_id');
	// if (body.default > 0) {
	// 	var bl = await db1.set({
	// 		user_id
	// 	}, {
	// 		default: 0
	// 	});
	// 	if (!bl) {
	// 		return $.ret.error(10000, '提交地址失败！');
	// 	}
	// }

	var m = query.method;
	var address_id = body.address_id;
	delete query.method;
	delete body.address_id;

	console.log("query", query, "body", body);

	console.log("执行类型: " + m);
	// 添加事件
	if (m === "add") {
		var bl = await db1.add(body);
		return bl > 0 ? $.ret.bl(true, '添加成功！') : $.ret.error(10000, '添加失败！');
	}
	// 删除事件
	else if (m === "del") {
		if (!address_id) {
			return $.ret.error(10000, '地址ID是必填字段！');
		}

		var obj = await db1.getObj({
			address_id,
			user_id
		});
		if (!obj) {
			return $.ret.error(10000, "对象不存在或不属于你");
		}

		var bl = await db1.del({
			address_id,
			user_id
		});
		console.log(bl);
		return bl > 0 ? $.ret.bl(true, '删除成功！') : $.ret.error(10000, '删除失败！');
	}
	// 修改事件
	else if (m === "set") {
		if (!address_id) {
			return $.ret.error(10000, '地址ID是必填字段！');
		}

		delete body.address_id;
		var obj = await db1.getObj({
			address_id,
			user_id
		});
		if (!obj) {
			return $.ret.error(10000, "对象不存在或不属于你");
		}

		var bl = await db1.set({
			address_id,
			user_id
		}, body);
		return bl > 0 ? $.ret.bl(true, '修改成功！') : $.ret.error(10000, '修改失败！');
	}
	// 错误返回
	else {
		return $.ret.error(10000, '错误的方法(method)！');
	}
};

exports.main = main;
