/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;
	var {
		ip,
		address
	} = body;

	if (ip) {
		if (ip == "127.0.0.1") {
			ip = ctx.ip
		}
		var db1 = db.new("user_access", "access_id");
		var now = new Date();
		var obj = await db1.getObj({
			ip,
			address,
			time_create_min: now.toStr('yyyy-MM-dd 00:00:00')
		});
		if (obj) {
			obj.times += 1;
		} else {
			await db1.add({
				ip,
				address,
				times: 1
			});
		}
	}
	return $.ret.bl(true, "post success!");
};

exports.main = main;
