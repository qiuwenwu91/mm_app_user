/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;
	var user = await this.get_state(ctx, db);
	query.user_id = user.user_id;
	// 操作sql模板
	var m = query.method;
	delete query.method;
	db.table = "user_account";
	var ret;
	if (m === "add") {
		var bl = await db.add(body);
		if (bl > 0) {
			ret = $.ret.bl(true, "添加成功！");
		} else {
			ret = $.ret.error(10000, "添加失败！原因：" + db.error.message);
		}
	} else if (m === "del") {} else if (m === "set") {
		if (body.username) {
			var obj = await db.getObj({
				username: body.username
			});
			if (obj && obj.user_id !== user.user_id) {
				return $.ret.error(10000, "修改失败！原因：账号已被其他人使用");
			}
		}
		var bl = await db.set(query, body);
		if (bl > 0) {
			ret = $.ret.bl(true, "修改成功！");
		} else {
			ret = $.ret.error(10000, "修改失败！原因：" + db.error.message);
		}
	} else if (m === "get_obj") {
		db.key = "user_id";
		var obj = await db.getObj(query);
		ret = $.ret.obj(obj);
	} else {
		var list = await db.get(query);
		ret = $.ret.list(list);
	}
	return ret;
};

exports.main = main;